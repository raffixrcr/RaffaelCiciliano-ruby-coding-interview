require 'rails_helper'

RSpec.describe "API Tweets", type: :request do
  describe "#create" do
    let(:response_body) { JSON.parse(response.body) }

    context 'with valid parameters' do
      let(:user1) { create(:user)}
      let(:valid_body) { 'This is a valid tweet' }

      it 'returns a successful response' do
        post api_tweets_path(user_id: user1.id, body: valid_body)

        expect(response).to have_http_status(:success)
      end

      it 'creates a new tweet' do
        expect {
          post api_tweets_path(user_id: user1.id, body: valid_body)
        }.to change(Tweet, :count).by(1)
      end
    end

    context 'With invalid parameters' do
      let(:user1) { create(:user)}

      context 'When the body is too long' do
        let(:invalid_body) { "Ethical wayfarers VHS, biodiesel gastropub aesthetic chillwave pabst fashion axe. Hoodie occupy kinfolk, poutine fingerstache 8-bit helvetica. Meditation heirloom PBR&B portland, farm-to-table mixtape leggings skateboard cliche. Celiac four loko helvetica, tofu deep v glossier man braid. Tote bag twee try-hard drinking vinegar crucifix, YOLO helvetica microdosing. Ennui farm-to-table tofu deep v, taxidermy leggings craft beer. Art party four loko iPhone, 3 wolf moon chia pour-over tacos sustainable chambray." }

        it 'returns an error response' do
          post api_tweets_path(user_id: user1.id, body: invalid_body)

          expect(response).to have_http_status(:bad_request)
        end

        it 'does not create a new tweet' do
          
        end
      end

      context 'When the tweet might be a duplicate' do

      end
    end
  end
end
