class Tweet < ApplicationRecord
  belongs_to :user

  validate :is_spam

  def is_spam
    if Tweet.where(body: body, user_id: user_id, created_at: (Time.now - 1.day)..).count != 0
      errors.add(:body, message: 'You already post this twitter in the last 24 hours.')
    end
  end
end
