class HomeController < ApplicationController
  def index
    @tweets = Tweet.includes(:users, :companies).distinct(:user_id).order("created_at DESC").limit(20).to_a
  end
end
